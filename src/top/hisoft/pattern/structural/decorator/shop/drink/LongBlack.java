package top.hisoft.pattern.structural.decorator.shop.drink;

public class LongBlack extends Coffee{
    public LongBlack() {
        setDescription("美式咖啡");
        setPrice(5.0f);
    }
}