package top.hisoft.pattern.structural.decorator.shop.drink;

public class Espresso extends Coffee{
    public Espresso() {
        setDescription("意大利咖啡");
        setPrice(6.0f);
    }
}
