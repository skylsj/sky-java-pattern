package top.hisoft.pattern.structural.decorator.shop.drink;

public class ShortBlack extends Coffee {
    public ShortBlack() {
        setDescription("浓缩咖啡");
        setPrice(4.0f);
    }
}
