package top.hisoft.pattern.structural.decorator.shop.drink;

public class Coffee extends Drink {
    @Override
    public float cost() {
        return super.getPrice();
    }
}
