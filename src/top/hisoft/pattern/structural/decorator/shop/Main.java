package top.hisoft.pattern.structural.decorator.shop;

import top.hisoft.pattern.structural.decorator.shop.decorate.Chocolate;
import top.hisoft.pattern.structural.decorator.shop.decorate.Milk;
import top.hisoft.pattern.structural.decorator.shop.drink.Drink;
import top.hisoft.pattern.structural.decorator.shop.drink.LongBlack;

public class Main {
    public static void main(String[] args) {
        Drink order = new LongBlack();
        System.out.println(order.cost()+"￥\t"+order.getDescription());
        order=new Milk(order);
        System.out.println(order.cost()+"￥\t"+order.getDescription());
        order=new Chocolate(order);
        System.out.println(order.cost()+"￥\t"+order.getDescription());
        order=new Chocolate(order);
        System.out.println(order.cost()+"￥\t"+order.getDescription());
    }
}
