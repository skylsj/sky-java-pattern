package top.hisoft.pattern.structural.decorator.shop.decorate;

import top.hisoft.pattern.structural.decorator.shop.drink.Drink;

public class Chocolate extends Decorator {
    public Chocolate(Drink drink) {
        super(drink);
        setDescription("巧克力");
        setPrice(3);
    }
}
