package top.hisoft.pattern.structural.decorator.shop.decorate;

import top.hisoft.pattern.structural.decorator.shop.drink.Drink;

public class Soy extends Decorator {
    public Soy(Drink drink) {
        super(drink);
        setDescription("豆浆");
        setPrice(1.5f);
    }
}