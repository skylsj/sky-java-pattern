package top.hisoft.pattern.structural.decorator.shop.decorate;

import top.hisoft.pattern.structural.decorator.shop.drink.Drink;

public class Milk  extends Decorator {
    public Milk(Drink drink) {
        super(drink);
        setDescription("牛奶");
        setPrice(2);
    }
}