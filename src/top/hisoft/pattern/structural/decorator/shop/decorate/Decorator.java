package top.hisoft.pattern.structural.decorator.shop.decorate;

import top.hisoft.pattern.structural.decorator.shop.drink.Drink;

public abstract class Decorator extends Drink {

    private Drink drink;

    public Decorator(Drink drink) {
        this.drink = drink;
    }

    @Override
    public float cost() {
        return super.getPrice() + drink.cost();
    }

    @Override
    public String getDescription() {
        return super.getDescription() + "&&" + drink.getDescription();
    }
}
