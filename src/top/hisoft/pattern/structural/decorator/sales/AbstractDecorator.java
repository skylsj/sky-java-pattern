package top.hisoft.pattern.structural.decorator.sales;

import java.math.BigDecimal;

/**
 * 定义抽象装饰者-创建参数构造方法，以便给具体构件增加功能。
 *
 * @author sky
 * @version 1.0.0
 * @since 2022-11-11
 */
public abstract class AbstractDecorator implements Goods {
    private final Goods goods;

    public AbstractDecorator(Goods goods) {
        this.goods = goods;
    }

    /**
     * 商品价格。
     *
     * @return 返回商品价格。
     */
    @Override
    public BigDecimal price() {
        return goods.price();
    }
}
