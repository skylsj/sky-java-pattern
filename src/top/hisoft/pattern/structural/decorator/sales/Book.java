package top.hisoft.pattern.structural.decorator.sales;

import java.math.BigDecimal;


/**
 * 定义具体构件-书
 *
 * @author sky
 * @version 1.0.0
 * @since 2022-11-11
 */
public class Book implements Goods{
    /**
     * 书的原价。
     *
     * @return 返回书的原价。
     */
    @Override
    public BigDecimal price() {
        return new BigDecimal(288.00);
    }
}
