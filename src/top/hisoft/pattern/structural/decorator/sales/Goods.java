package top.hisoft.pattern.structural.decorator.sales;

import java.math.BigDecimal;

/**
 * 定义抽象构件-商品
 *
 * @author sky
 * @version 1.0.0
 * @since 2022-11-11
 */
public interface Goods {

    /**
     * 商品价格。
     *
     * @return 返回商品价格。
     */
    BigDecimal price();
}
