package top.hisoft.pattern.structural.decorator.sales;

/**
 * @author sky
 */
public class Main {
    public static void main(String[] args) {
        Goods goods = new Book();
        System.out.println("书原价："+goods.price().intValue());
        goods = new ShopDiscountDecorator(goods);
        System.out.println("店铺折扣后："+goods.price().intValue());
        goods = new FullReductionDecorator(goods);
        System.out.println("满200减20后："+goods.price().intValue());
        goods = new CouponDecorator(goods);
        System.out.println("补贴券后："+goods.price().intValue());
    }
}
