package top.hisoft.pattern.structural.decorator.sales;

import java.math.BigDecimal;

/**
 * 定义具体装饰者-增加百亿补贴券50
 *
 * @author sky
 * @version 1.0.0
 * @since 2022-11-11
 */
public class CouponDecorator extends AbstractDecorator{


    public CouponDecorator(Goods goods) {
        super(goods);
    }

    /**
     * 商品价格。
     *
     * @return 返回商品价格。
     */
    @Override
    public BigDecimal price() {
        return super.price().subtract(new BigDecimal(50));
    }
}
