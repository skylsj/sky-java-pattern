package top.hisoft.pattern.structural.decorator.sales;

import java.math.BigDecimal;

/**
 * 定义具体装饰者-增加满200减20功能.
 *
 * @author sky
 * @version 1.0.0
 * @since 2022-11-11
 */
public class FullReductionDecorator extends AbstractDecorator {
    protected FullReductionDecorator(Goods goods) {
        super(goods);
    }

    /**
     * 商品价格。
     *
     * @return 返回商品价格。
     */
    @Override
    public BigDecimal price() {
        return super.price().subtract(new BigDecimal(20));
    }
}
