package top.hisoft.pattern.creational.singleton.inner;

import top.hisoft.pattern.creational.singleton.lazy.LazySingleton;
import top.hisoft.pattern.creational.singleton.lazy.LazySynchronizedSingleton;

/**
 * 6.静态内部类单例
 * <pre>
 *     1、采用类的装载机制保证初始化实例时只能一个实例。
 *     2、静态内部类在outer类装载时并不会立即实例化，只有在使用时（getInstance()）才会装载inner类。
 *     3、类的静态属性只会在第一次加载类的时候初始化，保证了线程的安全。
 * </pre>
 * <pre>
 * 优点：线程安全，利用静态内部类实现延迟加载，效率高
 * 结论：推荐使用
 * </pre>
 *
 * @author sky
 */
public class StaticInnerSingleton {

    private StaticInnerSingleton() {

    }

    private static class StaticInnerSingletonHolder {
        private static final StaticInnerSingleton _instance = new StaticInnerSingleton();
    }

    public static StaticInnerSingleton getInstance() {
        return StaticInnerSingletonHolder._instance;
    }
}
