package top.hisoft.pattern.creational.singleton;

import top.hisoft.pattern.creational.singleton.eager.EagerSingleton;
import top.hisoft.pattern.creational.singleton.eager.EagerStaticBlockSingleton;

/**
 * @author sky
 */
public class EagerMain {
    public static void main(String[] args) {
        EagerSingleton singleton1 = EagerSingleton.getInstance();
        EagerSingleton singleton2 = EagerSingleton.getInstance();
        System.out.println(singleton1 == singleton2);
        System.out.println("singleton1.hashCode: " + singleton1.hashCode());
        System.out.println("singleton2.hashCode: " + singleton2.hashCode());

        EagerStaticBlockSingleton blockSingleton1 = EagerStaticBlockSingleton.getInstance();
        EagerStaticBlockSingleton blockSingleton2 = EagerStaticBlockSingleton.getInstance();
        System.out.println(blockSingleton1 == blockSingleton2);
        System.out.println("blockSingleton1.hashCode: " + blockSingleton1.hashCode());
        System.out.println("blockSingleton2.hashCode: " + blockSingleton2.hashCode());
    }
}
