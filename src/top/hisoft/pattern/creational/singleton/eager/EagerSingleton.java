package top.hisoft.pattern.creational.singleton.eager;

/**
 * 1.饿汉式单例-静态变量。
 * <pre>
 * 优点：写法简单，在类装载时完成实例化，避免了线程同步问题
 * 缺点：没有达到Lazy Loading的效果，如果从未使用过个实例，就造成了内存的学浪费
 * 结论：可用，可能造成内存浪费
 *</pre>
 * @author sky
 * @since 2022-11-13
 */
public final class EagerSingleton {

    /**
     * 1.只有一个实例.
     */
    private static final EagerSingleton INSTANCE = new EagerSingleton();

    /**
     * 2.自己初始化.
     */
    private EagerSingleton() {
    }

    /**
     * 3.为系统提这一个实例.
     *
     * @return 唯一的实例.
     */
    public static EagerSingleton getInstance() {
        return INSTANCE;
    }
}
