package top.hisoft.pattern.creational.singleton.eager;

/**
 * 2.饿汉式单例-静态代码块
 * <pre>
 * 优点：写法简单，在类装载，执行静态代码块时完成实例化，避免了线程同步问题
 * 缺点：没有达到Lazy Loading的效果，如果从未使用过个实例，就造成了内存的学浪费
 * 结论：可用，可能造成内存浪费
 * </pre>
 *
 * @author sky
 * @since 2022-11-14
 */
public final class EagerStaticBlockSingleton {
    /**
     * 1.只有一个实例.
     */
    private static final EagerStaticBlockSingleton INSTANCE;

    /**
     * 静态块.
     */
    static {
        INSTANCE = new EagerStaticBlockSingleton();
    }

    /**
     * 2.自己初始化.
     */
    private EagerStaticBlockSingleton() {
    }

    /**
     * 3.为系统提这一个实例.
     *
     * @return 唯一的实例.
     */
    public static EagerStaticBlockSingleton getInstance() {
        return INSTANCE;
    }
}
