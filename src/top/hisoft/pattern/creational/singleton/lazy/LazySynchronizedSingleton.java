package top.hisoft.pattern.creational.singleton.lazy;

/**
 * 4.懒汉式单例-线程安全,性能差，有全局锁（同步方法）
 * <pre>
 *     优点：起到了Lazy Loading效果，解决了线程安全问题
 *     缺点：效率太低，每次都同步取实例。
 *     结论：在实际开发中可用，但不推荐使用。
 * </pre>
 *
 * @author sky
 */
public class LazySynchronizedSingleton {
    private static LazySynchronizedSingleton Instance;

    private LazySynchronizedSingleton() {

    }

    public static synchronized LazySynchronizedSingleton getInstance() {
        if (Instance == null) {
            Instance = new LazySynchronizedSingleton();
        }
        return Instance;
    }
}
