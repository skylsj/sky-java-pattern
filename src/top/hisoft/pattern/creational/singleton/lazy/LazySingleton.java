package top.hisoft.pattern.creational.singleton.lazy;

/**
 * 3.懒汉式单例-线程不安全
 * <pre>
 *     优点：起到了Lazy Loading效果，但只能在单线程下使用
 *     缺点：在多线程下会产生多个实例
 *     结论：在单线程下可以使用（fat app），在实际开发中(web)，不要使用这种方式。
 * </pre>
 *
 * @author sky
 */
public final class LazySingleton {

    private static LazySingleton _instance;

    private LazySingleton() {

    }

    public static LazySingleton getInstance() {
        if (_instance == null) {
            _instance = new LazySingleton();
        }
        return _instance;
    }
}
