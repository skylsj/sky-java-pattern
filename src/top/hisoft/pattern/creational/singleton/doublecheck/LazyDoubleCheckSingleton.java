package top.hisoft.pattern.creational.singleton.doublecheck;

import top.hisoft.pattern.creational.singleton.lazy.LazySingleton;

/**
 * 5.双重检查单例-优秀
 * <pre>
 *     优点：起到了Lazy Loading效果，解决了线程安全问题,保证了效率
 *     缺点：效率太低，每次都同步取实例。
 *     结论：在实际开发中，推荐使用。
 * </pre>
 * @author sky
 */
public class LazyDoubleCheckSingleton {

    private static volatile LazyDoubleCheckSingleton _instance;

    private LazyDoubleCheckSingleton() {

    }

    public static LazyDoubleCheckSingleton getInstance() {
        if (_instance == null) {
            synchronized (LazyDoubleCheckSingleton.class) {
                if (_instance == null) {
                    _instance = new LazyDoubleCheckSingleton();
                }
            }
        }
        return _instance;
    }
}
