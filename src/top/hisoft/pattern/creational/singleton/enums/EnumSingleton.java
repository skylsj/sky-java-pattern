package top.hisoft.pattern.creational.singleton.enums;

/**
 * 7.枚举单例
 * <pre>
 * 优点：线程安全，还能防止反序列化重新创建新的对象。
 * 结论：推荐使用
 * </pre>
 * <pre>
 * <code>
 *     public enum DataSourceEnum {
 *          DATASOURCE;
 *          private DBConnection connection = null;
 *          private DataSourceEnum() {
 *              connection = new DBConnection();
 *          }
 *          public DBConnection getConnection() {
 *              return connection;
 *          }
 *      }
 * </code>
 * </pre>
 *
 * @author sky
 */
public enum EnumSingleton {
    INSTANCE;

    public void say(String text) {
        System.out.println("Hello " + text);
    }
}
