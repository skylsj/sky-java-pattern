package top.hisoft.pattern.behavioral.strategy;

/**
 * 定义抽象策略角色-所有促销活动共同的接口
 *
 * @author sky
 * @since 2022-11-16
 */
public interface PromotionStrategy {
    /**
     * 显示促销内容。
     * @param name 促销名称。
     */
    void show(String name);
}
