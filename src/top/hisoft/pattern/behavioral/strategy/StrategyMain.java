package top.hisoft.pattern.behavioral.strategy;

/**
 * 客户端调用
 * @author sky
 * @since 2022-11-16
 */
public class StrategyMain {

    public static void main(String[] args) {
        PlatformContext context ;

        // 六月份
        String june="六月份";
        PromotionStrategy strategy = new SalePromotion618();
        context = new PlatformContext(strategy);
        context.platformShow(june);

        // 十一月份
        String november="十一月份";
        PromotionStrategy strategy11=new SalePromotion1111();
        context = new PlatformContext(strategy11);
        context.platformShow(november);

        // 十二月份
        String december = "十二月份";
        PromotionStrategy strategy12=new SalePromotion1212();
        context=new PlatformContext(strategy12);
        context.platformShow(december);
    }
}
