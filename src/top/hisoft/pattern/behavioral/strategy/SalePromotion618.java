package top.hisoft.pattern.behavioral.strategy;

/**
 * 定义具体策略角色-618大促策略
 *
 * @author sky
 * @since 2022-11-16
 */
public class SalePromotion618 implements PromotionStrategy {
    /**
     * 显示促销内容。
     *
     * @param name 促销名称。
     */
    @Override
    public void show(String name) {
        System.out.println(name + " 6.18大促");
    }
}
