package top.hisoft.pattern.behavioral.strategy;

/**
 * 定义环境角色-把促销的活动推送给用户。
 *
 * @author sky
 * @since 2022-11-16
 */
public class PlatformContext {
    private final   PromotionStrategy strategy;

    public PlatformContext(PromotionStrategy strategy) {
        this.strategy = strategy;
    }

    public void platformShow(String time){
        strategy.show(time);
    }
}
