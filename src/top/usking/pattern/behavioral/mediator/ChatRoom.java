package top.usking.pattern.behavioral.mediator;

import java.util.HashMap;
import java.util.Map;

public class ChatRoom implements Chat {

    private static final Map<String, Chatter> CHATTER_MAP = new HashMap<>();

    @Override
    public void send(String from, String to, String message) {
        Chatter chatterFrom = CHATTER_MAP.get(from);
        Chatter chatterTo = CHATTER_MAP.get(to);
        chatterTo.receiveMessage(chatterFrom, message);
    }

    @Override
    public void add(Chatter chatter) {
        chatter.setChat(this);
        CHATTER_MAP.put(chatter.getName(), chatter);
    }
}
