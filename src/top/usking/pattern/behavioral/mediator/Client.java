package top.usking.pattern.behavioral.mediator;

public class Client {
    public static void main(String[] args) {
        Chat chat = new ChatRoom();
        Chatter sky = new ChatterSky();
        Chatter tom = new ChatterTom();
        chat.add(sky);
        chat.add(tom);

        sky.send("Sky", "Tom", "Good morning.");
    }
}
