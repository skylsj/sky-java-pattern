package top.usking.pattern.behavioral.mediator;

public class ChatterTom extends Chatter {
    @Override
    public String getName() {
        return "Tom";
    }

    @Override
    public void receiveMessage(Chatter from, String message) {
        String result = String.format("From [%s] to [%s] send [%s]", from.getName(), getName(), message);
        System.out.println(result);
    }
}
