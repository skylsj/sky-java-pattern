package top.usking.pattern.behavioral.mediator;

public interface Chat {
    void send(String from,String to ,String message);
    void add(Chatter chatter);
}
