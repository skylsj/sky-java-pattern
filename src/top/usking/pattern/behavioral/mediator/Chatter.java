package top.usking.pattern.behavioral.mediator;

public abstract class Chatter {
    private Chat chat;

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public abstract String getName();

    public abstract void receiveMessage(Chatter from, String message);

    public void send(String form, String to, String message) {
        chat.send(form, to, message);
    }
}
