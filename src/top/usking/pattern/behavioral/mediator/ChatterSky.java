package top.usking.pattern.behavioral.mediator;

public class ChatterSky extends Chatter {

    @Override
    public String getName() {
        return "Sky";
    }

    @Override
    public void receiveMessage(Chatter from, String message) {
        String result = String.format("From [%s] to [%s] send [%s]", from.getName(), getName(), message);
        System.out.println(result);
    }
}
